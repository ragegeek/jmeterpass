#!/usr/bin/php
<?php

function parse_args($argv){
	//dump all cli args into $_GET so that the script could run via web or cli
	
	//debugging
	//print_r($argv);
	
	//convert to a key -> value array
	$ourargs = array();
	$ourargs["INTneedsequals"] = '';
	$lastvalue = '';
	for($i=0;$i<count($argv);$i++){
		$key = $argv[$i];
		//echo $i." ".$argv[$i]."\n"; //debug
		if(substr($key, 0, 1) == '-' && strpos($key, '=') === false){
			$ourargs[substr($key,1)] = "TRUE";
		}
		//if a flag has an argument, replace with actual value
		if(substr($lastvalue,0,1) == "-"){
				if(substr($key, 0, 1) != "-"){
					$ourargs[substr($lastvalue,1)] = $key;
				}
				if(strpos($lastvalue, '=') !== false){
					$tmp = explode('=', $lastvalue);
					$ourargs[substr($tmp[0],1)] = $tmp[1];
					$ourargs["INTneedsequals"] .= "*".trim(substr($tmp[0],1))."*";
					unset($tmp);
				}
		}
		//rotate dat shit
		$lastvalue = $key;
		
	}
		//little bit of cleanup from earlier
	unset($key);
	unset($lastvalue);		
	
	//Print argument array for debugging
	//print_r($ourargs);
	return $ourargs;
}

//allows detection of missing 'mandatory' arguments
//assumes comma seperated list in $requires
function mandatory_args($ourargs, $requires){
	$requirearray = explode(',', $requires);
	$missing = '';
	foreach($requirearray as $curcheck){
		if(!isset($ourargs[$curcheck])){
			$missing .= $curcheck."\n";
		}
	}
	return $missing;
}

//adds local args (not passed to jmeter)
//assumes comma seperated list in $localargs
function localargs($ourargs, $localargs){
	if(!isset($ourargs["INTlocalargs"])){
		$ourargs["INTlocalargs"] = "";
	}
	$ourargs["INTlocalargs"] .= $localargs;
	return $ourargs;
}

function gettestno($prefix){
	$checktestno = exec('ls |grep '.$prefix."test*");
	if($checktestno == ''){
		return 1;
	}else{
		$tmp = explode('.', exec('ls |grep '.$prefix."test*".'|tail -n 1'));
		for($i=0;$i<count($tmp);$i++){
			if(substr($tmp[$i], 0, 5) == "test0"){
				return intval(str_replace("test0", "0", $tmp[$i]))+1;
			}
		}
	}
}

function generate_jmeter_command($ourargs, $defaultcommand = ''){
	$gencommand = $defaultcommand;
	foreach($ourargs as $curcheck => $value){
		//echo $curcheck."\n";
		if(strpos($curcheck, "INT") === false){
			//echo $gencommand."\n";
			if(strpos($ourargs["INTneedsequals"], "*".trim($curcheck)."*") !== false){
				$gencommand .= ' -'.$curcheck.'='.$value." ";
			}elseif($value == "TRUE"){
				$gencommand .= ' -'.$curcheck." ";
			}else{
				$gencommand .= ' -'.$curcheck.' '.$value." ";
			}
			
		}
	}
	return $gencommand;
}

$ourargs = parse_args($argv);

//check for mandatory args
$missing = mandatory_args($ourargs, "t,Jhost,Jadmin_user,Jadmin_password,Jadmin_path,test,minusers,maxusers,increment");
if($missing != ''){
	die("missing one or more required arguments\n".$missing);
}

//minusers, maxusers, and increment all have to be integers
$minusers = intval($ourargs["minusers"]);
$maxusers = intval($ourargs["maxusers"]);
$increment = intval($ourargs["increment"]);

if($minusers == 0 || $maxusers == 0 || $increment == 0){
	echo $minusers."\n".$maxusers."\n".$increment."\n";
	die("a number-only value is incorrectly set\n");
}

// -test is going to be a "local" arg not passed to jmeter
$ourargs = localargs($ourargs, "test");
// if determine which is lower Jusers vs minusers and reset as the smallest
$jusers = intval($ourargs["Jusers"]);
if($jusers < $minusers){
	$minusers = $jusers;
}

$firstrun = true;
for($i = $minusers; $i <= $maxusers; $i += $increment){
	$ourargs["Jusers"] = $i;
	
	//start overriding variables
	$prefix = $ourargs["Jhost"].'.'.$ourargs["Jusers"].'.';
	$testno = $ourargs["test"];

	$curlog = $prefix."test".str_pad(strval($testno),4, '0', STR_PAD_LEFT).date(".Ymd.H.i").'.csv';
	$ourargs['l'] = $curlog;

	//print_r($ourargs);
	if($firstrun == false){
		echo "sleep for 5 minutes\n";
		//sleep(300);
	}else{
		$firstrun = false;
	}
	system(generate_jmeter_command($ourargs, '/opt/apache-jmeter-2.13/bin/jmeter'));
	echo "\n";
}
?>
